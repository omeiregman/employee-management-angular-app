import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { EMPLOYEES } from '../shared/mock-employees';
import { IEmployee } from '../shared/IEmployee';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  mockId = 45;
  getEmployeesService(): Observable<IEmployee[]> {
    return of(EMPLOYEES);
  }

  addEmployeeService(employee: IEmployee): Observable<number> {
    const employeeObject: IEmployee = { id: this.mockId, ...employee };
    return of(EMPLOYEES.push(employeeObject));
  }
}

/**
 * 
 * addEmployee(employee: Employee): Observable<number> {
  const listOfEmployees: Array<Employee> = EMPLOYEES;
  const id: number =
    listOfEmployees
      .map((employee: Employee) => employee.id)
      .sort()
      .reverse()[0] + 1;

  return of(
    EMPLOYEES.push({
      id,
      ...employee
    })
  );
  }
 */
